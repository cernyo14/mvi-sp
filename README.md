# CAFA 5 Protein Function Prediction
Semestrální práce do předmětu NI-MVI na téma CAFA 5 Protein Function Prediction. Zadání semestrální práce vyplývá ze soutěže na Kagglu s názvem [CAFA 5 Protein Function Prediction](https://www.kaggle.com/competitions/cafa-5-protein-function-prediction).

## Zadání a cíl semestrální práce
Cílem semestrální práce je predikovat funkci sady proteinů. Máme zadanou následující tabulku obsahující trénovací data.

| EntryID | term | aspect |
| :---:   | :---: | :---: |
| A0A009IHW8 | GO:0008152 | BPO |
| A0A009IHW8 | GO:0034655 | BPO |
| A0A009IHW8 | GO:0072523 | BPO |
| ... | ... | ... |

- `EntryID`: Značí ID daného proteinu
- `term`: Značí funkci daného proteinu (sloupec který se snažíme predikovat)
- `aspect`: Znači kategorii do které daný `term` spadá. Kategorie jsou pouze 3 (Molecular Function (MF), Biological Process (BP) a Cellular Component (CC)).

Zároveň máme k dispozici ze zadání z Kagglu dalsí informace/data. Máme k dispozici z jaké sekvence aminokyselin se každý protein (`EntryID`) skladá. Nebo také soubor, který blíže popisuje každý `term`.

Jelikož jeden protein (`EntryID`) může mít více funkcí (`termů`) jedná se o multi-label klasifikační úlohu.

## Styl řešení
Práce je řešena pomocí dvou modelů a to MLP a 1D CNN s využitím předtrénovaného protein embeddingu. Na Kagglu je dostupných více různých protein embeddingů přesně pro naši úlohu. My vuyžijeme tyto embeddingy jakožto vstupní data do dašich modelů, abychom se pokusily predikovat dané `termy`.

## Obsah Repozitáře
Z důvodu velikosti vstupních dat nejsou data nahrána na tento repozitář a tudíž není ani `cafa-5-protein-function-multiple-embedds.ipynb` notebook spustitelný.
- `cafa-5-protein-function-multiple-embedds.ipynb`: Jupyter notebook s řešením semestrální práce. Notebook by měl být včetně outputů buněk. Počet epoch a počet natrénovanách modelů v rámci optimalizace hyperparametů je snížen pro účely prezentace notebooku. Jinak by nebylo možné spustit všechny buňky noteboku z důvodu náročnosti na pamět.
- `report.pdf`: Report semestrální práce.
